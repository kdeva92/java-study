package java8.streamStudy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.print.attribute.standard.MediaSize.Other;

public class MyClass {

	public static void main(String[] args) {
		List<Integer> ilist = Arrays.asList(1, 2, 3, 4, 5);
		Optional<Integer> result = ilist.stream().max((x, y) -> x - y);
		System.out.println(result.get());

		result = ilist.stream().min(Comparator.comparingInt(x -> x));
		System.out.println(result.get());

		//filter out nos less than 2, sort and print all nos
		ilist.stream().filter(x -> x > 2).sorted().collect(Collectors.toList()).forEach(System.out::print);
		
		System.out.println("unique");
		ilist.stream().distinct().collect(Collectors.toList()).forEach(System.out::print);
		
		//filter all values above 3 and square add them
		List<Integer> intList = new ArrayList<Integer>(500);
		for(int i=0;i<500;i++)
			intList.add(i);
		int sum = intList.parallelStream().filter(x-> x>3).mapToInt(Integer::new).map(x -> x*x).sum(); //reduce((x,y)->x+y);
		System.out.println("\nSum is: "+sum);
		
		IntSummaryStatistics summary = intList.stream().mapToInt(Integer::new).summaryStatistics();
		System.out.println("Summary: "+summary);
		
		// for (Iterator iterator = sortedList.iterator(); iterator.hasNext();)
		// {
		// Integer integer = (Integer) iterator.next();
		// System.out.print(integer+" ");
		// }
		
		List<String> sList = Arrays.asList("a", "b", "f", "d", "e", "c");
		Optional<String> sResult = sList.stream().max(String::compareTo);
		System.out.println(sResult.get());

		sResult = sList.stream().max((x, y) -> x.compareTo(y));
		System.out.println(sResult.get());

		sResult = sList.stream().sorted((x, y) -> x.compareTo(y) * -1).reduce((x, y) -> x + y);
		System.out.println(sResult.get());

	}
}