package java8.defaultMethods;

//If a class implements two interfaces having same signatured default methods, the class MUST override
//the default method and provide its own implementation
//While referring to implemented interface, InterfaceName.super has to be used.. simple super is superclass
public class ImplementTwoDefaultInterfaces implements DemoDefaultInterface, AnotherDefaultInterfcae {

	@Override
	public void normalMethod() {
		
		//btw.. one cannot simply directly access var defined in both interfaces without explicit
		//reference to it as DemoDefaultInterface.i
		//System.out.println("i: "+i);
	}

	@Override
	public void defaultMethod() {
		// TODO Auto-generated method stub
		AnotherDefaultInterfcae.super.defaultMethod();
	}

}
