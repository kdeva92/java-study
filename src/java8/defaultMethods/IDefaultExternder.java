package java8.defaultMethods;

public interface IDefaultExternder extends DemoDefaultInterface {

	@Override
	default void defaultMethod() {
		// TODO Auto-generated method stub
		//DemoDefaultInterface.super.defaultMethod();
		System.out.println("In overridden Default method ");
		System.out.println(this.getClass());
	}
}
