package java8.defaultMethods;

public interface AnotherDefaultInterfcae {

	int i =8;
	public default void defaultMethod() {
		System.out.println("Default method of anotherInterface");
	}
	
	public default void defaultMethod(int i) {
		System.out.println("Default method of anotherInterface");
	}
}
