package java8.defaultMethods;


//An interface having default methods can still be functional if it has only one non-default method
//
@FunctionalInterface
public interface DemoDefaultInterface {

	int i = 7;
	public void normalMethod();
	
	public default void defaultMethod() {
		System.out.println("Default method of interface");
	}
	
	public static void staticMethod() {
		System.out.println("Static method in interfce");
	}
	
}
