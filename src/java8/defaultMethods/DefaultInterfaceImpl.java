package java8.defaultMethods;


//Default methods enable you to add new functionality to the interfaces of your libraries and ensure 
//binary compatibility with code written for older versions of those interfaces.

//QUESTION: if in precompiled code there is a method as same signature as default method but with different
//return type -- big time fail possible

// No need to provide impl for default methods.. interface impl is considered
//default methods can be overridden


//In case of dimond inheritance ie. C and D inherit from A, and D overrides default method but C does not,
//the call will go to implementation of D and not A --> that is closest implementer
public class DefaultInterfaceImpl implements IDefaultExternder{ //DemoDefaultInterface {

	@Override
	public void normalMethod() {
		// TODO Auto-generated method stub
		
	}

	//comment to see call to interface method or uncomment call in impl
	public void defaultMethod(){
		System.out.println("Default method of Class");
		//DemoDefaultInterface.super.defaultMethod();
	}
	
	
	public static void main(String[] args) {
		DemoDefaultInterface interfaceRef = new DefaultInterfaceImpl();
		interfaceRef.defaultMethod();
		
		DefaultInterfaceImpl classRef = new DefaultInterfaceImpl();
		classRef.defaultMethod();

		DemoDefaultInterface.staticMethod();
	}

}
