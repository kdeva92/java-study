package java8.lambda.basics;

import java.util.Arrays;
import java.util.List;



public class MyClass {

	static int staticInt = 20;
	int instanceInt = 20;
	
	
	//simple return of instance of interface.
	public MyFunctionalInterface myClassMyMethod1() {
		// no need of explicit return statement here
		MyFunctionalInterface myF = (x) -> "Hello "+x;
		return myF;
	}
	
	//the instance of interface has access to local variables of enclosing method and variables of class
	public MyFunctionalInterface myClassMyMethod2() {
		int localInt = 30;
		//valid implementation without code block {} and return statement
		//MyFunctionalInterface myF = x ->"var: "+(x+localInt + instanceInt+staticInt);
		
		//all local variable must be "final or effectively final" - any change in value of local variable before or after lambda is compilation error in lambda
		//Behavior similar to static method => look at lambda as static method
		//localInt = 20;

		
		//also a valid implementation that has explicit function body {}
		//can access and mutate static and instance variables of enclosing class
		//can access but CANNOT mutate local variables -> both props similar to anonymous inner class 
		MyFunctionalInterface myF = x ->{staticInt= 50; System.out.println("In lambda"); return "var: "+(x+localInt + instanceInt+staticInt);};
		return myF;
	}
	
	//working very similar to anonymous inner classes
	public MyFunctionalInterface methodGivingAnonymousClass() {
		int localInt = 30;
		return new MyFunctionalInterface() {
			@Override
			public String myFunction(int x) {
				staticInt = 50;
				return "var: "+(x+localInt + instanceInt+staticInt);
			}
		};
	}
	
	//Inner classes vs lambda expressions
	//inner classes can have any no of methods but lambda can have only one declared in functional interface
	
	//use of "this" - this here refers to the instance invoking the lambda - unlike inner class where this is object of class
	//we CAN use the this of enclosing type in similar way as used in inner classes 
	//We cannot have instance variables in lambda -> because it is not a class
	public MyFunctionalInterface myClassMyMethod3() {
		MyFunctionalInterface myF = (x) ->{ 
			//private int var =10;
			//static int var2 = 50;
			System.out.println("accessing instance variable with this of caller "+ this.instanceInt);
			System.out.println("accessing instance variable with this of caller "+ MyClass.this.instanceInt);
			return "Hello "+x;};
		return myF;
	}
	

	//to access "this" of enclosing type need to use "ClassName.this"
	public MyFunctionalInterface methodGivingAnonymousClass2() {
		int localInt = 30;
		return new MyFunctionalInterface() {
			@Override
			public String myFunction(int x) {
				staticInt = 50;
				//System.out.println("accessing instance variable with this of caller "+ this.instanceInt);
				System.out.println("accessing instance variable with this of caller "+ MyClass.this.instanceInt);
				return "var: "+(x+localInt + instanceInt+staticInt);
			}
		};
	}

	
	
	public static void main(String[] args) {

		MyClass myClass = new MyClass();
		MyFunctionalInterface myF1 = myClass.myClassMyMethod1();
		System.out.println(myF1.myFunction(20));
		
		MyFunctionalInterface myF2 = myClass.myClassMyMethod2();
		System.out.println(myF2.myFunction(20));
		
		MyFunctionalInterface myFFromClass = myClass.methodGivingAnonymousClass();
		System.out.println(myFFromClass.myFunction(20));
		
		MyFunctionalInterface myF3 = myClass.myClassMyMethod3();
		System.out.println(myF3.myFunction(20));
	}
	
}
