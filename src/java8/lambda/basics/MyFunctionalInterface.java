package java8.lambda.basics;

@FunctionalInterface
//identifier optional
public interface MyFunctionalInterface {

	public String myFunction(int x);
	//error by @FunctionalInterface if interface not functional
	//public String method2();
}
