package java8.lambda.methodReference;

import java.util.function.Consumer;
import java.util.function.Function;

public class MethodReferenceDemo {

	//java.util.function provides various types of functional interfaces to be used and 
	//no need to write interface every time
	
	
	interface I{
		public Integer getInt(String s);
	}
	
	static class MySampleClass implements I{

		public MySampleClass(Integer i) {
			System.out.println("in constructor of i");
		}

		@Override
		public Integer getInt(String s) {
			// TODO Auto-generated method stub
			return Integer.parseInt(s);
		}
	}
	
	public static void myStaticMethod(int i) {
		System.out.println("Static method param "+i);
	}
	
	public static void main(String[] args) {
		
		//simple lambda for java.util.Consumer
		Consumer<Integer> consumer = x -> System.out.println(x);
		consumer.accept(12);
		
		//-----------------------------
		
		//call to existing static function
		consumer = MethodReferenceDemo::myStaticMethod;
		consumer.accept(12);
		
		Function<String,Double> funcParseDouble = Double::parseDouble;
		Double result = funcParseDouble.apply("2.12");
		System.out.println("result of parseDouble: "+result);
		
		
		//------------------------------
		
		//referencing a constructor ??????
		//referencing a constructor - constructor called and new instance is created
		consumer = MySampleClass::new;
		consumer.accept(12);
		//consumer = new MySampleClass(5); 
		
		Function<String, Double> func = Double::new;
	 	result = func.apply("2.12");
		System.out.println("Double is: "+result);

		//-------------------------------
		
		//refer to instance methods of other objects
		Consumer<String> sConsumer = System.out::println;
		sConsumer.accept("Hello World!!");

		//-------------------------------
		
				
		//refer instance methods of passed objects
		Function<String, String> toUpper = String::toUpperCase;
		String upper = toUpper.apply("hello world!!");
		System.out.println("Uppercase: "+upper);
		
	}
	
}
